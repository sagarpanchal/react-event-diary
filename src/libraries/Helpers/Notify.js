import { store } from 'react-notifications-component'
import NativeNotification from './NativeNotification'

const DEFAULT_TIMEOUT = 6000

class Notify {
  static defaults = {
    type: 'info',
    insert: 'top',
    container: 'top-right',
    animationIn: ['animated', 'fadeInRightBig', 'fast'],
    animationOut: ['animated', 'fadeOutRightBig', 'fast'],
    dismiss: { duration: DEFAULT_TIMEOUT },
    dismissable: { click: true, onScreen: true },
    extra: { override: false, native: true }
  }

  static notify = async (type, title, message, options = {}) => {
    const { extra, ...rest } = { ...Notify.defaults, ...options }
    const { native } = { ...extra, native: !document.hasFocus() }
    store.addNotification({ ...rest, type, title, message })
    if (native) await new NativeNotification(title, message, type).push()
  }

  static demo = async (
    title = 'Lorem',
    message = 'Lorem ipsum dolor sit amet.'
  ) => {
    const options = { dismiss: { duration: DEFAULT_TIMEOUT * 100 } }
    for (const type of ['info', 'success', 'warn', 'error']) {
      await new Promise(r => setTimeout(r, DEFAULT_TIMEOUT / 6))
      await Notify[type](title, message, options)
    }
  }

  static typeFactory = type => (title, message, options = {}) =>
    Notify.notify(type, title, message, options)

  static info = Notify.typeFactory('info')

  static success = Notify.typeFactory('success')

  static warn = Notify.typeFactory('warning')

  static error = Notify.typeFactory('danger')
}

window.Notify = Notify

export default Notify
