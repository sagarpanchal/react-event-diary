import { store } from 'store'
import i18n from 'i18n'

const translateKey = (p, o, s) =>
  p.split('.').reduce((a, b) => (!s ? a[b] : a ? a[b] : undefined), o)

const supplant = (r, p) => {
  for (var q in p) r = r.replace(new RegExp(`{${q}}`, 'g'), p[q])
  return r
}

/**
 * Translate text
 * @param {String} k key
 * @param {Object} p placeholder
 */
export default (key, placeholder, locale) => {
  const l = locale || store.getState().intl.locale
  const r = (this || translateKey)(key, i18n[l]['messages'])
  return typeof placeholder === 'undefined' ? r : supplant(r, placeholder)
}
