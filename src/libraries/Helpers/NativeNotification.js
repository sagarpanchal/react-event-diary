import Push from 'push.js'

const DEFAULT_TIMEOUT = 6000

window.NativeNotification = class NativeNotification {
  constructor(title, body, type, timeout = DEFAULT_TIMEOUT * 10) {
    this.Push = Push
    this.title = title
    this.body = body
    this.type = type
    this.timeout = timeout
    this.icon = `/assets/images/notifications/${type}.png`
  }

  onClick() {
    window.focus()
    this.close()
  }

  push() {
    return this.Push.create(this.title, {
      body: this.body,
      icon: this.icon,
      timeout: this.timeout,
      onClick: this.onClick
    })
  }
}

export default window.NativeNotification
