/**
 * Scroll inside an element to specified position
 *
 * @param   {String}          element
 * @param   {ScrollToOptions} options
 * @returns {void}
 */
const scrollTo = (element, options) => {
  element = document.querySelector(element)
  const { top, left, behavior } = options
  const scroll = {}
  void (top && (scroll.top = top))
  void (left && (scroll.left = left))
  void (behavior && (scroll.behavior = behavior))

  try {
    element.scrollTo(options)
  } catch (_) {
    void (top && (element.scrollTop = top))
    void (left && (element.scrollLeft = left))
  }
}

export default scrollTo
