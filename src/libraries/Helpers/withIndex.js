/**
 * Returns index of the object by _id
 * overwrites duplicate values
 * @param   {Object} object
 * @param   {String} prop property
 * @returns {Object}
 */
export const withIndex = (array = [], prop = '_id') => {
  let object = {}
  array.forEach(item => (object[item[prop]] = { ...item }))
  return object
}

/**
 * Returns index of the object by specified property
 * won't overwrite duplicate values
 * @param   {Object} object
 * @param   {String} prop property
 * @returns {Object}
 */
export const createIndexByProp = (array = [], prop, idField = '_id') => {
  let object = {}
  array.forEach(item => {
    if (Object.prototype.hasOwnProperty.call(object, item[prop]))
      object[item[prop]] = []
    object[item[prop]].push(item[idField])
  })
  return object
}
