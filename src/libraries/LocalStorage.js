import { languages } from 'i18n'

export default class LocalStorage {
  static setLang = lang => {
    !Object.keys(languages).includes(lang) &&
      (lang = navigator.language.split('-')[0] || 'en')
    localStorage.setItem('lang', lang)
  }

  static getLang = () => localStorage.getItem('lang') || 'en'

  static clearLang = () => localStorage.removeItem('lang')
}
