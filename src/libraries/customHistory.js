import { createBrowserHistory } from 'history'

window.globals = window.globals || {}

window.globals.history = window.globals.history || createBrowserHistory()

export default window.globals.history
