import React from 'react'
import PropTypes from 'prop-types'

const Spinner = ({ show, noBackdrop }) =>
  show ? (
    <>
      {!noBackdrop && (
        <div className="custom-backdrop show" style={{ zIndex: 1055 }} />
      )}
      <div
        className="loading-spinner spinner-border"
        role="status"
        style={{ zIndex: 1054 }}
      >
        <span className="sr-only">Loading...</span>
      </div>
    </>
  ) : null

Spinner.propTypes = {
  show: PropTypes.bool,
  noBackdrop: PropTypes.bool
}

export default Spinner
