import React from 'react'
import { NavLink as NLink } from 'react-router-dom'
import { Link } from 'react-router-dom'
import Nav from 'reactstrap/lib/Nav'
import NavItem from 'reactstrap/lib/NavItem'
import NavLink from 'reactstrap/lib/NavLink'
import Translate from 'components/Translate'
import Navbar from 'components/Navbar'

class Header extends React.Component {
  state = { navIsOpen: false }

  navToggle = () => this.setState({ navIsOpen: !this.state.navIsOpen })

  render = () => {
    const { navIsOpen } = this.state

    const navs = (
      <Nav className="ml-auto" navbar>
        <NavItem>
          <NavLink tag={NLink} exact to="/about-me" onClick={this.navToggle}>
            <Translate value="menu.aboutMe" />
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={NLink} exact to="/settings" onClick={this.navToggle}>
            <Translate value="menu.settings" />
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={NLink} exact to="/logout" onClick={this.navToggle}>
            <Translate value="menu.logout" />
          </NavLink>
        </NavItem>
      </Nav>
    )

    const brand = (
      <div className={navs ? '' : 'col-12 p-0 text-center'}>
        <Link to="/" className="navbar-brand">
          <span>React Event Diary</span>
        </Link>
      </div>
    )

    return (
      <Navbar
        color="black"
        offCanvas
        brand={brand}
        navs={navs}
        isOpen={navIsOpen}
        toggle={this.navToggle}
      />
    )
  }
}

export default Header
