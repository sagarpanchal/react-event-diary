import React from 'react'
import PropTypes from 'prop-types'
import Spinner from 'components/Spinner'
import debounce from 'lodash/debounce'
import history from 'libraries/customHistory'

class Loader extends React.Component {
  static propTypes = { animation: PropTypes.string }

  unlisten = undefined
  loader = React.createRef()
  animation = this.props.animation || 'fadeOut'

  scrollUp = () => {
    const container = document.querySelector('.container-fluid')
    void (container && (container.scrollTop = 0))
  }

  show = () => {
    this.loader.current.classList.remove('d-none')
  }

  hide = debounce(() => {
    this.scrollUp()
    this.loader.current.classList.add(this.animation)
    this.loader.current.classList.add('animated')
    setTimeout(() => {
      this.loader.current.classList.add('d-none')
      this.loader.current.classList.remove(this.animation)
      this.loader.current.classList.remove('animated')
    }, 320)
  }, 160)

  animate = () => void (this.show(), this.hide())

  componentDidMount = () => {
    window.addEventListener('DOMContentLoaded', this.show)
    window.addEventListener('load', this.hide)
    this.unlisten = history.listen(() => this.animate())
  }

  componentWillUnmount = () => {
    window.removeEventListener('DOMContentLoaded', this.show)
    window.removeEventListener('load', this.hide)
    void (this.unlisten && this.unlisten())
  }

  render = () => {
    return (
      <div className="custom-loader" ref={this.loader}>
        <Spinner show noBackdrop />
      </div>
    )
  }
}

export default Loader
