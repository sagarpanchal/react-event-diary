import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as Yup from 'yup'
import { Formik, Form } from 'formik'
import { Input, Select, Textarea } from 'formik-reactstrap'
import Button from 'reactstrap/lib/Button'
import Card from 'reactstrap/lib/Card'
import CardHeader from 'reactstrap/lib/CardHeader'
import CardBody from 'reactstrap/lib/CardBody'
import Translate from 'components/Translate'

const validationSchema = Yup.object().shape({
  date: Yup.date()
    .required('Event date is required')
    .typeError('Event date is invalid'),
  type: Yup.mixed()
    .oneOf(['Session', 'Professional'], 'Invalid Event')
    .required('Event type is required')
    .typeError('Event type is invalid'),
  note: Yup.string()
    .trim()
    .min(1, 'Add more details')
    .required('Address is required')
    .typeError('Address must be a string')
})

const initialValues = {
  date: new Date().toISOString().split('T')[0],
  type: 'Session',
  note: ''
}

export default class EventForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func
  }

  render = () => {
    const { onSubmit } = this.props

    return (
      <Card className="mt-3 border-black">
        <CardHeader className="text-white font-weight-bold bg-black border-black">
          <Translate value="title.addEvent" />
        </CardHeader>
        <CardBody>
          <Formik {...{ validationSchema, initialValues, onSubmit }}>
            {props => (
              <Form>
                <div className="row">
                  <div className="col">
                    <Input
                      title={<Translate value="field.eventDate" />}
                      name="date"
                      type="date"
                      formikProps={props}
                    />
                    <Select
                      title={<Translate value="field.eventType" />}
                      name="type"
                      options={[
                        { text: 'Session', value: 'Session' },
                        { text: 'Personal', value: 'Personal' }
                      ]}
                      formikProps={props}
                    />
                  </div>
                  <div className="col" style={{ minWidth: '70%' }}>
                    <Textarea
                      title={<Translate value="field.eventNote" />}
                      name="note"
                      formikProps={props}
                      style={{ minHeight: '5.8em' }}
                    />
                  </div>
                </div>
                <Button
                  color="black"
                  size="sm"
                  type="submit"
                  name="submit"
                  className="float-right"
                >
                  <Translate value="button.addEvent" />
                </Button>
              </Form>
            )}
          </Formik>
        </CardBody>
      </Card>
    )
  }
}
