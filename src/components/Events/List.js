import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Card from 'reactstrap/lib/Card'
import CardBody from 'reactstrap/lib/CardBody'
import Table from 'reactstrap/lib/Table'
import Translate from 'components/Translate'

export default class EventList extends Component {
  static propTypes = {
    events: PropTypes.array
  }

  render = () => {
    const { events } = this.props

    return (
      events.length > 0 && (
        <Card className="mt-3 border-black">
          <CardBody className="p-0">
            <Table responsive className="m-0">
              <thead className="bg-black text-light">
                <tr>
                  <th style={{ width: '8em' }}>
                    <Translate value="field.eventDate" />
                  </th>
                  <th style={{ width: '8em' }}>
                    <Translate value="field.eventType" />
                  </th>
                  <th>
                    <Translate value="field.eventNote" />
                  </th>
                </tr>
              </thead>
              <tbody>
                {events.map((event, n) => (
                  <tr key={n}>
                    <td>{event.date}</td>
                    <td>{event.type}</td>
                    <td>{event.note}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      )
    )
  }
}
