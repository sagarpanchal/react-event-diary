import React, { Component } from 'react'
import EventForm from 'components/Events/Form'
import EventList from 'components/Events/List'

export default class Events extends Component {
  state = { events: [] }

  addEvent = (event, formikBag) => {
    this.setState({ events: [...this.state.events, event] })
    formikBag.resetForm()
  }

  render = () => {
    const { events } = this.state

    return (
      <>
        <EventForm {...{ onSubmit: this.addEvent }} />
        <EventList {...{ events }} />
      </>
    )
  }
}
