import React from 'react'
import PropTypes from 'prop-types'
import OModal from 'reactstrap/lib/Modal'
import ModalHeader from 'reactstrap/lib/ModalHeader'
import ModalBody from 'reactstrap/lib/ModalBody'
import ModalFooter from 'reactstrap/lib/ModalFooter'

const Modal = ({ isOpen, toggle, size, className, header, body, footer }) => {
  void (!size && (size = 'md'))
  void (!className && (className = 'modal-dialog-centered'))
  return (
    <OModal {...{ isOpen, toggle, size, className }}>
      {header && <ModalHeader {...{ toggle, children: header }} />}
      {body && <ModalBody {...{ children: body }} />}
      {footer && <ModalFooter {...{ children: footer }} />}
    </OModal>
  )
}

const children = PropTypes.oneOfType([
  PropTypes.arrayOf(PropTypes.node),
  PropTypes.node
])

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  size: PropTypes.string,
  className: PropTypes.string,
  header: children,
  body: children,
  footer: children
}

export default Modal
