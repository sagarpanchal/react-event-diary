import React from 'react'
import PropTypes from 'prop-types'
import NavBar from 'reactstrap/lib/Navbar'
import Collapse from 'reactstrap/lib/Collapse'
import NavbarToggler from 'reactstrap/lib/NavbarToggler'

class Navbar extends React.Component {
  static childrenType = PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])

  static propTypes = {
    color: PropTypes.string,
    brand: this.childrenType,
    navs: this.childrenType,
    offCanvas: PropTypes.bool,
    isOpen: PropTypes.bool,
    toggle: PropTypes.func
  }

  render = () => {
    const { color, brand, navs, offCanvas, isOpen, toggle } = this.props
    const size = 'sm'
    const backdropClasses = offCanvas
      ? ['custom-backdrop', 'd-block', 'd-' + size + '-none']
      : null
    const collapseClasses = offCanvas
      ? ['navbar-collapse', 'offcanvas-collapse', 'bg-' + color]
      : null

    if (offCanvas)
      isOpen
        ? collapseClasses.push('open') && backdropClasses.push('show')
        : collapseClasses.push('collapsed') && backdropClasses.push('fade')

    return (
      <>
        {navs &&
          offCanvas &&
          isOpen && (
            <div className={backdropClasses.join(' ')} onClick={toggle} />
          )}
        <NavBar color={color} dark expand={size} fixed="top">
          <div className="container">
            {brand}
            {navs && <NavbarToggler onClick={toggle} />}
            {navs &&
              (offCanvas ? (
                <div className={collapseClasses.join(' ')}>{navs}</div>
              ) : (
                <Collapse isOpen={isOpen} navbar>
                  {navs}
                </Collapse>
              ))}
          </div>
        </NavBar>
      </>
    )
  }
}

export default Navbar
