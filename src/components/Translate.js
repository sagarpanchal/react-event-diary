import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import translate from 'libraries/Helpers/translate'

const Translate = ({ value, placeholders, locale }) => (
  <>{translate(value, placeholders, locale)}</>
)

Translate.propTypes = {
  value: PropTypes.string.isRequired,
  placeholders: PropTypes.object,
  locale: PropTypes.string
}

const mapState = ({ intl: { locale } }) => ({ locale })

export default connect(mapState)(Translate)
