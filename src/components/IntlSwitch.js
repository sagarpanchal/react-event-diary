import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { setLocale } from 'actions/intl'
import { languages } from 'i18n'
import LS from 'libraries/LocalStorage'

const IntlSwitch = ({ locale, setLocale }) => (
  <footer className="position-fixed" style={{ bottom: 0, right: 0 }}>
    <select
      value={locale}
      onChange={({ target: { value: v } }) => setLocale(v) && LS.setLang(v)}
    >
      {Object.keys(languages).map(key => (
        <option value={key} key={key}>
          {languages[key]}
        </option>
      ))}
    </select>
  </footer>
)

IntlSwitch.propTypes = { locale: PropTypes.string, setLocale: PropTypes.func }

const mapState = ({ intl: { locale } }) => ({ locale })
const mapDispatch = { setLocale }

export default connect(mapState, mapDispatch)(IntlSwitch)
