import { intlActionTypes } from 'reducers/intl'

const { SET } = intlActionTypes

export const setLocale = locale => ({ type: SET, locale })
