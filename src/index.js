import ReactDOM from 'react-dom'
import React from 'react'
import App from 'App'
import { store } from 'store'
import { Provider } from 'react-redux'
import { sagaMiddleware } from 'middleware'
import rootSaga from 'sagas'
import 'libraries/FontAwesome'
import 'sass/app.scss'
// import * as serviceWorker from './serviceWorker'

sagaMiddleware.run(rootSaga)

ReactDOM.render(
  <Provider {...{ store }}>
    <App />
  </Provider>,
  document.getElementById('root')
)

// serviceWorker.unregister()
