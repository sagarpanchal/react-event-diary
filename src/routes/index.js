import React from 'react'
import { Route, Switch } from 'react-router'
import Events from 'components/Events'
import NotFound from 'routes/NotFound'

const Routes = () => (
  <Switch>
    <Route path="/" exact component={Events} />
    <Route component={NotFound} />
  </Switch>
)

export default Routes
