import data from './lang'

export const languages = ((l = {}) => {
  data.forEach(({ key, name }) => (l[key] = name))
  return l
})()

const translations = ((l = {}) => {
  data.forEach(({ key, messages }) => (l[key] = { locale: key, messages }))
  return l
})()

export default translations
