import { all } from 'redux-saga/effects'
import { sagaInit } from './init'

function* watcherSaga() {}

export default function* rootSaga() {
  yield all([watcherSaga(), sagaInit()])
}
