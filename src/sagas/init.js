import { put } from 'redux-saga/effects'
import { setLocale } from 'actions/intl'
import LS from 'libraries/LocalStorage'

export function* sagaInit() {
  LS.setLang(LS.getLang())
  yield put(setLocale(LS.getLang()))
}
