import React from 'react'
import { Router } from 'react-router-dom'
import Header from 'components/Header'
import Loader from 'components/Loader'
import Notifier from 'react-notifications-component'
import IntlSwitch from 'components/IntlSwitch'
import history from 'libraries/customHistory'
import Routes from 'routes'

export default class App extends React.Component {
  render = () => {
    return (
      <Router history={history}>
        <>
          <Header />
          <div className="container custom-scrollbar">
            <Loader />
            <Notifier />
            <Routes />
            <IntlSwitch />
          </div>
        </>
      </Router>
    )
  }
}
