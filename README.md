# Setup Instructions

### Clone this repo in current directory

**Create and switch to a new empty directory and run following command**

```sh
git clone https://gitlab.com/sagarpanchal/react-event-diary.git
```

**Install all dependencies**

```sh
npm i   # install required npm packages
```

**Run the project**

```sh
npm start   # for development
npm run build   # for production build
```
